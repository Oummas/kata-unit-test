const sum = require('./sum');

test('Given 0, should sums 0', () => {
  const given = '0';

  const expected = 0;

  const actual = sum(given);

  expect(actual).toEqual(expected);
});
